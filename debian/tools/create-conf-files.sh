#!/bin/bash
#
# Script to generate configuration files for each Percona-Toolkit binary
#
set -e

cd ../../bin
BINLIST="percona-toolkit "
BINLIST+=$(ls -1 pt-*)
#echo ${BINLIST}
OUTPUTDIR=../debian/etc/percona-toolkit

#
# Remove existing
#
rm -frv ../debian/etc/percona-toolkit/pt-*

for f in ${BINLIST}
do
	#echo $(pwd)
	#echo ${f}
	CONFFILE=${OUTPUTDIR}/${f}.conf

	echo "Creating config file: ${CONFFILE} ..."
	if [ ${f} = "percona-toolkit" ] ; then
		echo "#" > ${CONFFILE}
		echo "# ${f}.conf: Global configuration file for *ALL TOOLS* in ${f} package" >> ${CONFFILE}
		echo "#" >> ${CONFFILE}
		echo "noversion-check" >> ${CONFFILE}
	else
		echo "#" > ${CONFFILE}
		echo "# ${f}.conf: Global tool-specific configuration file for ${f}" >> ${CONFFILE}
		echo "#" >> ${CONFFILE}
	fi

done

