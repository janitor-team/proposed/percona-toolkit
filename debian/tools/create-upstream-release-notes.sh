#!/bin/bash
#
# This script looks for any upstream release notes file that matches 
# the current package version and creates a 'debian/RELEASENOTES' file 
# from it.
#
# The current package version is extracted from Makefile.PL file.
#
# Usually, upstream stores release notes files under the 'docs' 
# directory and the filename contains a mangled version number.
#
# ie: Release notes filename for version 3.0.12 is docs/rn.3-0-12.txt
#
# This script is prepared to be called from inside 'debian/rules' by 
# the 'override_dh_installdocs' target.
#

set -e

echo "****************************************************************"

VERSION=$(grep "VERSION " Makefile.PL | awk -F "'" {'print $2'})

echo "Detected version: ${VERSION}"

VERSION=${VERSION//./-}
UPSTREAM_RELEASE_NOTES=$(echo "docs/rn.${VERSION}.txt")
DEBIAN_RELEASE_NOTES="debian/RELEASENOTES"

echo "Removing ${DEBIAN_RELEASE_NOTES} from previous executions ..."
rm -fv ${DEBIAN_RELEASE_NOTES}


if [ ! -f ${UPSTREAM_RELEASE_NOTES} ]; then
    echo "File not found!"
else
	echo "Upstream release notes filename: ${UPSTREAM_RELEASE_NOTES}"
	cp -v ${UPSTREAM_RELEASE_NOTES} ${DEBIAN_RELEASE_NOTES}
fi

echo "****************************************************************"
