percona-toolkit (3.2.1-1) unstable; urgency=medium

  * New upstream release (3.2.1) 
  * debian/patches/01-fix-spelling-errors.diff: refreshed

 -- Dario Minnucci <midget@debian.org>  Sun, 30 Aug 2020 12:55:55 +0200

percona-toolkit (3.2.0-1) unstable; urgency=medium

  * New upstream release (3.2.0)
  * Refresh debian/patches/01-fix-spelling-errors.diff
  * Manually add fixes reported by Janitor's MR !1 at:
    https://salsa.debian.org/debian/percona-toolkit/-/merge_requests/1
    - Use secure URI in Homepage field.
    - Set debhelper-compat version in Build-Depends.
    - Update standards version to 4.5.0, no changes needed.

 -- Dario Minnucci <midget@debian.org>  Thu, 07 May 2020 01:37:32 +0200

percona-toolkit (3.1+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No-change source-only upload to allow testing migration.

 -- Boyuan Yang <byang@debian.org>  Fri, 11 Oct 2019 13:44:22 -0400

percona-toolkit (3.1+dfsg-1) unstable; urgency=medium

  * New upstream release (3.1)
  * Repack original tarball to remove binary: pt-mongodb-query-digest
  * debian/control:
    - Bump debhelper compatibility to 12
    - Bump Standards-Version to 4.4.0 (no changes)
  * debian/copyright:
    - Update copyright years.
    - Remove duplicated text on recurring licensed files.

 -- Dario Minnucci <midget@debian.org>  Fri, 27 Sep 2019 01:01:25 +0200

percona-toolkit (3.0.13-1) unstable; urgency=medium

  * New upstream release (3.0.13)
  * debian/control:
    - Bump Standards-Version to 4.3.0 (no changes)

 -- Dario Minnucci <midget@debian.org>  Sun, 03 Feb 2019 20:28:19 +0100

percona-toolkit (3.0.12-1) unstable; urgency=medium

  * New upstream release (3.0.12)
  * debian/control:
    - Bump Standards-Version to 4.2.1 (no changes)
  * debian/patches:
    - Add 01-fix-spelling-errors.diff to fix some spelling errors.
  * Fix some 'file-contains-trailing-whitespace' linian issues.
  * debian/tools:
    - Add maintainer script (create-upstream-release-notes.sh) to be able
      to install upstream's release notes under the package documentation
      directory (/usr/share/doc/percona-toolkit/RELEASENOTES)

 -- Dario Minnucci <midget@debian.org>  Tue, 18 Sep 2018 05:37:30 +0200

percona-toolkit (3.0.11-1) unstable; urgency=medium

  * New upstream release (3.0.11)

 -- Dario Minnucci <midget@debian.org>  Thu, 12 Jul 2018 16:12:04 +0200

percona-toolkit (3.0.10-1) unstable; urgency=medium

  * New upstream release (3.0.10)
  * debian/copyright: Update copyright years.

 -- Dario Minnucci <midget@debian.org>  Wed, 23 May 2018 18:46:33 +0200

percona-toolkit (3.0.9-1) unstable; urgency=medium

  * New upstream release (3.0.9)
  * debian/control:
    - Bump debhelper compatibility to 11
    - Bump Standards-Version to 4.1.4 (no changes)
  * debian/doc: Remove README file installation

 -- Dario Minnucci <midget@debian.org>  Thu, 03 May 2018 11:06:02 +0200

percona-toolkit (3.0.8+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.8)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
    - .travis.yml
    - util/mysql_random_data_loader_linux_amd64
    - util/version_cmp

 -- Dario Minnucci <midget@debian.org>  Sun, 18 Mar 2018 14:34:27 +0100

percona-toolkit (3.0.7+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.7)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
    - .travis.yml
    - util/mysql_random_data_loader_linux_amd64
    - util/version_cmp

 -- Dario Minnucci <midget@debian.org>  Wed, 14 Mar 2018 15:25:14 +0100

percona-toolkit (3.0.6+dfsg-2) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.1.3:
      - debian/patches/00-fix-perl-shebang.diff: Fix perl shebangs.

 -- Dario Minnucci <midget@debian.org>  Wed, 14 Feb 2018 13:32:59 +0100

percona-toolkit (3.0.6+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.6)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
    - .travis.yml
    - util/mysql_random_data_loader_linux_amd64
  * debian/control:
    - Move git repo to 'salsa.debian.org'.
      Update Vcs-Git and Vcs-Browser fields.

 -- Dario Minnucci <midget@debian.org>  Tue, 13 Feb 2018 11:39:00 +0100

percona-toolkit (3.0.5+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.5)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
    - .travis.yml
  * debian/control:
    - Bump Standards-Version to 4.1.1 (no changes)

 -- Dario Minnucci <midget@debian.org>  Sun, 26 Nov 2017 12:02:14 +0100

percona-toolkit (3.0.4+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.4)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
  * debian/control:
    - Add dependency on fdisk | util-linux (<< 2.29.2-3~)
      Thanks to Andreas Henriksson. (Closes: 872214)

 -- Dario Minnucci <midget@debian.org>  Sat, 19 Aug 2017 03:37:31 +0200

percona-toolkit (3.0.3+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.3)
  * Repack original tarball to remove some files and dirs:
    - .git
    - .gitignore
    - config/deb/control.bak
    - config/rpm/percona-toolkit.spec.bak
  * debian/copyright: Update copyright years and other fixes.

 -- Dario Minnucci <midget@debian.org>  Fri, 04 Aug 2017 21:04:00 +0200

percona-toolkit (3.0.2+dfsg-1) unstable; urgency=medium

  * New upstream release (3.0.2)
  * Repack original tarball to remove .git metadata (.git directory
    and .gitignore file)
  * debian/watch:
    - Look for sources under GitHub
    - Add dversionmangle option to ignore '+dfsg' part
  * debian/control:
    - Bump Standards-Version to 4.0.0 (no changes)
    - Bump debhelper compatibility to 10
  * debian/missing-sources:
    - Add sources for jQuery 1.4.2

 -- Dario Minnucci <midget@debian.org>  Sat, 29 Jul 2017 16:10:12 +0200

percona-toolkit (2.2.20-1) unstable; urgency=medium

  * New upstream release (2.2.20)
  * Remove patch debian/patches/001-spelling-error-in-manpages.diff.
    Applied upstream.

 -- Dario Minnucci <midget@debian.org>  Mon, 02 Jan 2017 14:51:07 +0100

percona-toolkit (2.2.19-1) unstable; urgency=medium

  * New upstream release (2.2.19)
  * Add preconfigured /etc/percona-toolkit/percona-toolkit.conf file
    to disable the 'version-check' functionality which is enabled by
    default in upstream code.
    This method is valid to disable the 'version-check' functionality
    on other percona-toolkit versions. (Closes: #826728)
  * debian/control:
    - Fix Vcs-Git and Vcs-Browser URLs

 -- Dario Minnucci <midget@debian.org>  Tue, 22 Nov 2016 15:11:22 +0100

percona-toolkit (2.2.18-1) unstable; urgency=low

  * New upstream release (2.2.18)

 -- Dario Minnucci <midget@debian.org>  Thu, 28 Jul 2016 11:06:11 +0200

percona-toolkit (2.2.17-1) unstable; urgency=low

  * New upstream release (2.2.17)
  * debian/control: Bump Standards-Version to 3.9.8 (no changes)
  * debian/copyright: Update copyright years

 -- Dario Minnucci <midget@debian.org>  Sun, 08 May 2016 15:25:35 +0200

percona-toolkit (2.2.16-1) unstable; urgency=low

  * New upstream release (2.2.16)

 -- Dario Minnucci <midget@debian.org>  Tue, 10 Nov 2015 13:52:15 +0100

percona-toolkit (2.2.15-1) unstable; urgency=low

  * New upstream release (2.2.15)

 -- Dario Minnucci <midget@debian.org>  Wed, 09 Sep 2015 13:50:04 +0200

percona-toolkit (2.2.14-1) unstable; urgency=low

  * New upstream release (2.2.14)
  * debian/copyright:
    - Remove paragraph refering to no longer distributed 'pt-collect'
    - Remove paragraph refering to no longer distributed 'pt-log-player'
    - Remove paragraph refering to no longer distributed 'pt-query-advisor'
    - Remove paragraph refering to no longer distributed 'pt-tcp-model'
    - Remove paragraph refering to no longer distributed 'pt-trend'

 -- Dario Minnucci <midget@debian.org>  Thu, 16 Apr 2015 12:20:34 +0200

percona-toolkit (2.2.13-1) unstable; urgency=medium

  * New upstream release (2.2.13)

 -- Dario Minnucci <midget@debian.org>  Wed, 04 Feb 2015 19:03:23 +0100

percona-toolkit (2.2.12-1) unstable; urgency=medium

  * New upstream release (2.2.12)
  * debian/control: Bump Standards-Version to 3.9.6 (no changes)
  * debian/copyright: Update copyright years
  * debian/watch: Fix download URL

 -- Dario Minnucci <midget@debian.org>  Tue, 27 Jan 2015 16:49:41 +0100

percona-toolkit (2.2.11-1~dfsg1) unstable; urgency=medium

  * New upstream release (2.2.11)
  * Sources repacked to remove provided 'debian' directory. Package
    tagged as '~dfsg1'

 -- Dario Minnucci <midget@debian.org>  Fri, 26 Sep 2014 21:53:20 +0200

percona-toolkit (2.2.10-1) unstable; urgency=medium

  * New upstream release (2.2.10)
  * debian/copyright: Copyright years update

 -- Dario Minnucci <midget@debian.org>  Sun, 17 Aug 2014 00:39:16 +0200

percona-toolkit (2.2.9-1~dfsg1) unstable; urgency=medium

  * New upstream release (2.2.9)
  * Sources repacked to remove provided 'debian' directory. Package
    tagged as '~dfsg1'

 -- Dario Minnucci <midget@debian.org>  Thu, 10 Jul 2014 18:01:36 +0200

percona-toolkit (2.2.8-1~dfsg1) unstable; urgency=medium

  * New upstream release (2.2.8)
  * Sources repacked to remove provided 'debian' directory. Package
    tagged as '~dfsg1'

 -- Dario Minnucci <midget@debian.org>  Wed, 04 Jun 2014 17:39:15 +0200

percona-toolkit (2.2.7-1~dfsg1) unstable; urgency=high

  * New upstream release (2.2.7)
  * Sources repacked to remove provided 'debian' directory. Package
    tagged as '~dfsg1'
  * Fix for CVE-2014-2029: --version-check behaves like spyware.
    (Closes: #740846)

 -- Dario Minnucci <midget@debian.org>  Wed, 05 Mar 2014 21:32:01 +0100

percona-toolkit (2.2.6-1) unstable; urgency=low

  * New upstream release (2.2.6)
  * debian/control: Bump Standards-Version to 3.9.5 (no changes)

 -- Dario Minnucci <midget@debian.org>  Mon, 23 Dec 2013 17:37:40 +0100

percona-toolkit (2.2.5-1) unstable; urgency=low

  * New upstream release (2.2.5)

 -- Dario Minnucci <midget@debian.org>  Mon, 21 Oct 2013 11:21:06 +0200

percona-toolkit (2.2.4-1) unstable; urgency=low

  * New upstream release (2.2.4)

 -- Dario Minnucci <midget@debian.org>  Mon, 22 Jul 2013 12:54:30 +0200

percona-toolkit (2.2.3-1) unstable; urgency=low

  * New upstream release (2.2.3)

 -- Dario Minnucci <midget@debian.org>  Tue, 25 Jun 2013 00:12:07 +0200

percona-toolkit (2.2.2-1) unstable; urgency=low

  * New upstream release (2.2.2)

 -- Dario Minnucci <midget@debian.org>  Mon, 06 May 2013 23:50:29 +0200

percona-toolkit (2.2.1-1) unstable; urgency=low

  * New upstream release (2.2.1)
  * debian/patches/001-spelling-error-in-manpages.diff: Fix for error
    in manpages. Thanks to Benjamin Kerensa. (Closes: #698116)
  * debian/control: Canonical name in Vcs-Git and Vcs-Browser fields

 -- Dario Minnucci <midget@debian.org>  Wed, 20 Mar 2013 17:36:59 +0100

percona-toolkit (2.1.9-1) unstable; urgency=low

  * New upstream release (2.1.9)
  * debian/copyright: Years updated
  * debian/control: Bump Standards-Version to 3.9.4 (no changes)

 -- Dario Minnucci <midget@debian.org>  Thu, 21 Feb 2013 02:04:56 +0100

percona-toolkit (2.1.8-1) unstable; urgency=low

  * New upstream release (2.1.8) (Closes: #698185)

 -- Dario Minnucci <midget@debian.org>  Mon, 21 Jan 2013 17:36:54 +0100

percona-toolkit (2.1.7-1) unstable; urgency=low

  * New upstream release (2.1.7)

 -- Dario Minnucci <midget@debian.org>  Tue, 20 Nov 2012 12:33:11 +0100

percona-toolkit (2.1.6-1) unstable; urgency=low

  * New upstream release (2.1.6) (Closes: #693389)

 -- Dario Minnucci <midget@debian.org>  Mon, 19 Nov 2012 18:34:08 +0100

percona-toolkit (2.1.5-1) unstable; urgency=low

  * New upstream release (2.1.5)

 -- Dario Minnucci <midget@debian.org>  Wed, 17 Oct 2012 05:02:22 +0200

percona-toolkit (2.1.4-1) unstable; urgency=low

  * New upstream release (2.1.4) (Closes: #689061)

 -- Dario Minnucci <midget@debian.org>  Sun, 30 Sep 2012 20:43:04 +0200

percona-toolkit (2.1.3-1) unstable; urgency=low

  * New upstream release (2.1.3)

 -- Dario Minnucci <midget@debian.org>  Fri, 10 Aug 2012 20:19:08 +0200

percona-toolkit (2.1.2-1) unstable; urgency=low

  * New upstream release (2.1.2)
  * debian/watch: Changes to match only versions starting with digits.
  * debian/patches: Drop patches (applied upstream)
    - 000-Makefile.PL-fix-manpage-section-mismatch.diff
    - 000-docs-percona-toolkit.pod-fix-manpage-has-bad-whatis-entry.diff

 -- Dario Minnucci <midget@debian.org>  Fri, 15 Jun 2012 23:10:40 +0200

percona-toolkit (2.1.1-1) unstable; urgency=low

  * New upstream release (2.1.1)

 -- Dario Minnucci <midget@debian.org>  Sat, 07 Apr 2012 00:25:17 +0200

percona-toolkit (2.0.4-1) unstable; urgency=low

  * New upstream release (2.0.4)
  * debian/patches:
    - 000-Makefile.PL-fix-manpage-section-mismatch.diff: Refreshed
  * debian/control:
    - Bump Standards-Version to 3.9.3 (no changes)
    - Update Build-Depends to use debhelper (>= 9)

 -- Dario Minnucci <midget@debian.org>  Sat, 10 Mar 2012 08:40:03 +0100

percona-toolkit (2.0.3-1) unstable; urgency=low

  * debian/watch: Updated to match new download URL.
  * debian/patches:
    - 000-Makefile.PL-fix-manpage-section-mismatch.diff: Refreshed.
    - 000-pt-table-sync-use-of-uninitialized-value-3055.diff: Dropped,
      applied upstream.

 -- Dario Minnucci <midget@debian.org>  Fri, 10 Feb 2012 23:27:39 +0100

percona-toolkit (2.0.2-1) unstable; urgency=low

  * New upstream release (2.0.2)
  * debian/patches:
    - Drop 000-pt-tcp-model-broken-for-type-requests.diff (Fixed upstream)
    - 000-Makefile.PL-fix-manpage-section-mismatch.diff (Updated)

 -- Dario Minnucci <midget@debian.org>  Mon, 09 Jan 2012 17:10:35 +0100

percona-toolkit (1.0.1-3) unstable; urgency=low

  * 000-pt-table-sync-use-of-uninitialized-value-3055.diff:
    Fix for "Use of uninitialized value $args{"chunk_range"} ..."
    (Closes: #650409)

 -- Dario Minnucci <midget@debian.org>  Tue, 29 Nov 2011 17:24:12 +0100

percona-toolkit (1.0.1-2) unstable; urgency=low

  * debian/copyright: Percona Inc. email addess updated.
  * debian/patches/000-pt-tcp-model-broken-for-type-requests.diff:
    Fix for pt-tcp-model brekages when used with --type=requests
    option. Thanks to Kristian Grønfeldt Sørensen for the patch and
    Daniel Nichter for confirmation. (Closes: 648820)

 -- Dario Minnucci <midget@debian.org>  Fri, 18 Nov 2011 03:10:04 +0100

percona-toolkit (1.0.1-1) unstable; urgency=low

  [Dario Minnucci]
  * Initial release (Closes: #642344)
  * debian/copyright: Updated.
  * debian/watch: Added.
  * debian/patches:
    + 000-docs-percona-toolkit.pod-fix-manpage-has-bad-whatis-entry.diff
    + 000-Makefile.PL-fix-manpage-section-mismatch.diff

 -- Dario Minnucci <midget@debian.org>  Thu, 22 Sep 2011 23:59:56 +0200
